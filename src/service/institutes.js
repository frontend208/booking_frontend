import api from './api'

export function getAllInstitute () {
  return api.get('/institutes')
}

export function getInstitute (id) {
  return api.get('/institutes/' + id)
}

export function addInstitute () {
  return api.post('/institutes')
}

export function updateInstitute (id) {
  return api.put('/institutes/' + id)
}

export function deleteInstitute (id) {
  return api.delect('/institutes/' + id)
}
