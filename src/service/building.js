import api from './api'

export function getAllBuildings () {
  console.log(api.get('/buildings'))
  return api.get('/buildings')
}

export function getBuilding (id) {
  return api.get('/buildings/a/' + id)
}

export function addBuilding () {
  return api.post('/buildings')
}

export function updateBuilding (id) {
  return api.put('/buildings/' + id)
}

export function deleteBuilding (id) {
  return api.delete('/buildings/' + id)
}
