import api from './api'

export function getAllUsers () {
  return api.get('/users')
}

export function getUser (id) {
  return api.get('/users/' + id)
}

export function addUser () {
  return api.post('/users')
}

export function updateUser (id) {
  return api.put('/users/' + id)
}

export function deleteUser (id) {
  return api.delect('/users/' + id)
}
