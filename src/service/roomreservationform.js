import api from './api'

export function getAllRoomReservationForms () {
  return api.get('/roomReservationForms')
}
export function getRoomReservationFormsByUser (id) {
  return api.get('/roomReservationForms/user/' + id)
}
export function deleteRoomReservationForm (id) {
  return api.delete('/roomReservationForms/' + id)
}
export function addRoomReservationForms (form) {
  return api.post('/roomReservationForms/', form)
}
