import api from './api'

export function getAllRoom () {
  return api.get('/rooms')
}

export function getRooms (id) {
  return api.get('/rooms/' + id)
}

export function addRoom () {
  return api.post('/rooms')
}

export function updateRoom (id) {
  return api.put('/rooms/' + id)
}

export function deleteRoom (id) {
  return api.delect('/rooms/' + id)
}
