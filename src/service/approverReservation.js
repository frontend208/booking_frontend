import api from './api'

export function getAllApproverReservations () {
  return api.get('/approverReservationsRouter')
}

export function getApproverReservations (id) {
  return api.get('/approverReservationsRouter')
}
