import { AUTH_LOGIN, AUTH_LOGOUT } from '../mutation-types'
import router from '../../router'
import { login } from '../../service/auth'
import { getUser } from '../../service/user'

export default {
  namespaced: true,
  state: () => ({
    user: localStorage.getItem('user')
      ? JSON.parse(localStorage.getItem('user'))
      : null
  }),

  mutations: {
    [AUTH_LOGIN] (state, payload) {
      state.user = payload
    },
    [AUTH_LOGOUT] (state) {
      state.user = null
    }
  },
  actions: {
    async login ({ commit }, payload) {
      try {
        const res = await login(payload.email, payload.password)
        const user = res.data.user
        const token = res.data.token

        console.log(user._id)

        localStorage.setItem('token', token)
        localStorage.setItem('user', JSON.stringify(user))
        const currentUser = await getUser(user._id)
        localStorage.setItem('name', JSON.stringify(currentUser.data.name))
        localStorage.setItem(
          'surname',
          JSON.stringify(currentUser.data.surname)
        )
        localStorage.setItem(
          'institute',
          JSON.stringify(currentUser.data.institute)
        )
        console.log(res)
        router.push('/home')
        commit(AUTH_LOGIN, user)
      } catch (err) {
        console.log('err')
      }
    },
    logout ({ commit }) {
      window.localStorage.clear()
      commit(AUTH_LOGOUT)
    }
  },
  getters: {
    isLogin (state, getters) {
      return state.user != null
    }
  }
}
